﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns
{
    public interface IMyClonable<T>
    {
        T Copy();
    }
}
