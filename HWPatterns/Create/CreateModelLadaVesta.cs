﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки и модели
    public class CreateModelLadaVesta : CreateAutoLada, IMyClonable<CreateModelLadaVesta>, ICloneable
    {
        private string Engine { get; set; }
        private string Equipment { get; set; }
        private int ColorId { get; set; }

        public CreateModelLadaVesta(string engine, int vin) : base(vin)
        {
            Engine = engine;
            Console.WriteLine($"Создан автомобиль Lada Vesta c двигателем: {Engine}");
            ColorId = new Random().Next();
        }

        public void SetEquipment(string equi)
        {
            Equipment = equi;
        }

        public int GetColorId()
        {
            return ColorId;
        }

        public CreateModelLadaVesta Copy()
        {
            var copy = new CreateModelLadaVesta(Engine, Vin);
            copy.Equipment = Equipment;
            copy.ColorId = ColorId;
            return copy;
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
