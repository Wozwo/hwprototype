﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки
    public class CreateAutoLada : CreateAuto
    {
        public int Vin { get; set; }

        public CreateAutoLada(int vin)
        {
            Vin = vin;
        }

        public void GetVin()
        {
            Console.WriteLine(Vin);
        }
    }
}
