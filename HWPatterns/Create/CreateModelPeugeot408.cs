﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки и модели
    public class CreateModelPeugeot408 : CreateAutoPeugeot, IMyClonable<CreateModelPeugeot408>, ICloneable
    {
        private string Engine { get; set; }
        private string Equipment { get; set; }
        private int ColorId { get; set; }

        public CreateModelPeugeot408(string engine, int vin) : base(vin)
        {
            Engine = engine;
            Console.WriteLine($"Создан автомобиль Peugeot 408 c двигателем: {Engine}");
            ColorId = new Random().Next();
        }

        public void SetEquipment(string equi)
        {
            Equipment = equi;
        }

        public int GetColorId()
        {
            return ColorId;
        }

        public CreateModelPeugeot408 Copy()
        {
            var copy = new CreateModelPeugeot408(Engine, Vin);
            copy.Equipment = Equipment;
            copy.ColorId = ColorId;
            return copy;
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
