﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки и модели
    public class CreateModelLadaGranta : CreateAutoLada, IMyClonable<CreateModelLadaGranta>, ICloneable
    {
        private string Engine { get; set; }
        private string Equipment { get; set; }
        private int ColorId { get; set; }

        public CreateModelLadaGranta(string engine, int vin) : base(vin)
        {
            Engine = engine;
            Console.WriteLine($"Создан автомобиль Lada Granta c двигателем: {Engine}");
            ColorId = new Random().Next();
        }

        public void SetEquipment(string equi)
        {
            Equipment = equi;
        }

        public int GetColorId()
        {
            return ColorId;
        }

        public CreateModelLadaGranta Copy()
        {
            var copy = new CreateModelLadaGranta(Engine, Vin);
            copy.Equipment = Equipment;
            copy.ColorId = ColorId;
            return copy;
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
