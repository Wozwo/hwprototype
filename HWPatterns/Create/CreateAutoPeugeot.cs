﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки
    public class CreateAutoPeugeot : CreateAuto
    {
        public int Vin { get; set; }

        public CreateAutoPeugeot(int vin)
        {
            Vin = vin;
        }

        public void GetVin()
        {
            Console.WriteLine(Vin);
        }
    }
}
