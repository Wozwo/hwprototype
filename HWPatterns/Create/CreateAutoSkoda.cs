﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки
    public class CreateAutoSkoda : CreateAuto
    {
        public int Vin { get; set; }

        public CreateAutoSkoda(int vin)
        {
            Vin = vin;
        }

        public void GetVin()
        {
            Console.WriteLine(Vin);
        }
    }
}
