﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWPatterns.Create
{
    //Класс создания автомобиля конкретной марки и модели
    public class CreateModelPeugeot3008 : CreateAutoPeugeot, IMyClonable<CreateModelPeugeot3008>, ICloneable
    {
        private string Engine { get; set; }
        private string Equipment { get; set; }
        private int ColorId { get; set; }

        public CreateModelPeugeot3008(string engine, int vin) : base(vin)
        {
            Engine = engine;
            Console.WriteLine($"Создан автомобиль Peugeot 3008 c двигателем: {Engine}");
            ColorId = new Random().Next();
        }

        public void SetEquipment(string equi)
        {
            Equipment = equi;
        }

        public int GetColorId()
        {
            return ColorId;
        }

        public CreateModelPeugeot3008 Copy()
        {
            var copy = new CreateModelPeugeot3008(Engine, Vin);
            copy.Equipment = Equipment;
            copy.ColorId = ColorId;
            return copy;
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
