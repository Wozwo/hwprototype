﻿using HWPatterns.Create;
using System;

namespace HWPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            MyClonable();
            Console.WriteLine("\nUse base clonable interface\n");
            BaseClonable();
        }

        private static void MyClonable()
        {
            var vesta = new CreateModelLadaVesta("Бензин 1.6", new Random().Next(1000000, 9999999));
            var granta = new CreateModelLadaGranta("Бензин 1.6", new Random().Next(1000000, 9999999));
            var p3008 = new CreateModelPeugeot3008("Бензин Турбо 1.6", new Random().Next(1000000, 9999999));
            var p408 = new CreateModelPeugeot408("Бензин 1.6", new Random().Next(1000000, 9999999));
            var octavia = new CreateModelSkodaOctavia("Бензин Турбо 1.8", new Random().Next(1000000, 9999999));
            var rapid = new CreateModelSkodaRapid("Бензин 1.6", new Random().Next(1000000, 9999999));

            var copyVesta = vesta.Copy();
            var copyGranta = granta.Copy();
            var copyP3008 = p3008.Copy();
            var copyP408 = p408.Copy();
            var copyOctavia = octavia.Copy();
            var copyRapid = rapid.Copy();

            Console.WriteLine($"Созданная Vesta: цвет {vesta.GetColorId()} и vin {vesta.Vin}  Копия Vesta: цвет {copyVesta.GetColorId()} и vin {copyVesta.Vin}");
            Console.WriteLine($"Созданная Granta: цвет {granta.GetColorId()} и vin {granta.Vin}  Копия Granta: цвет {copyGranta.GetColorId()} и vin {copyGranta.Vin}");
            Console.WriteLine($"Созданная 3008: цвет {p3008.GetColorId()} и vin {p3008.Vin}  Копия 3008: цвет {copyP3008.GetColorId()} и vin {copyP3008.Vin}");
            Console.WriteLine($"Созданная 408: цвет {p408.GetColorId()} и vin {p408.Vin}  Копия 408: цвет {copyP408.GetColorId()} и vin {copyP408.Vin}");
            Console.WriteLine($"Созданная Octavia: цвет {octavia.GetColorId()} и vin {octavia.Vin}  Копия Octavia: цвет {copyOctavia.GetColorId()} и vin {copyOctavia.Vin}");
            Console.WriteLine($"Созданная Rapid: цвет {rapid.GetColorId()} и vin {rapid.Vin}  Копия Rapid: цвет {copyRapid.GetColorId()} и vin {copyRapid.Vin}");

            Console.WriteLine("\nИзменяем vin у оригиналов\n");

            vesta.Vin = 123456789;
            granta.Vin = 123456789;
            p3008.Vin = 123456789;
            p408.Vin = 123456789;
            octavia.Vin = 123456789;
            rapid.Vin = 123456789;

            Console.WriteLine($"Созданная Vesta: vin {vesta.Vin}  Копия Vesta: vin {copyVesta.Vin}");
            Console.WriteLine($"Созданная Granta: vin {granta.Vin}  Копия Granta: vin {copyGranta.Vin}");
            Console.WriteLine($"Созданная 3008: vin {p3008.Vin}  Копия 3008: vin {copyP3008.Vin}");
            Console.WriteLine($"Созданная 408: vin {p408.Vin}  Копия 408: vin {copyP408.Vin}");
            Console.WriteLine($"Созданная Octavia: vin {octavia.Vin}  Копия Octavia: vin {copyOctavia.Vin}");
            Console.WriteLine($"Созданная Rapid: vin {rapid.Vin}  Копия Rapid: vin {copyRapid.Vin}");
        }

        private static void BaseClonable()
        {
            var vesta = new CreateModelLadaVesta("Бензин 1.6", new Random().Next(1000000, 9999999));
            var granta = new CreateModelLadaGranta("Бензин 1.6", new Random().Next(1000000, 9999999));
            var p3008 = new CreateModelPeugeot3008("Бензин Турбо 1.6", new Random().Next(1000000, 9999999));
            var p408 = new CreateModelPeugeot408("Бензин 1.6", new Random().Next(1000000, 9999999));
            var octavia = new CreateModelSkodaOctavia("Бензин Турбо 1.8", new Random().Next(1000000, 9999999));
            var rapid = new CreateModelSkodaRapid("Бензин 1.6", new Random().Next(1000000, 9999999));

            var copyVesta = (CreateModelLadaVesta)vesta.Clone();
            var copyGranta = (CreateModelLadaGranta)granta.Clone();
            var copyP3008 = (CreateModelPeugeot3008)p3008.Clone();
            var copyP408 = (CreateModelPeugeot408)p408.Clone();
            var copyOctavia = (CreateModelSkodaOctavia)octavia.Clone();
            var copyRapid = (CreateModelSkodaRapid)rapid.Clone();

            Console.WriteLine($"Созданная Vesta: цвет {vesta.GetColorId()} и vin {vesta.Vin}  Копия Vesta: цвет {copyVesta.GetColorId()} и vin {copyVesta.Vin}");
            Console.WriteLine($"Созданная Granta: цвет {granta.GetColorId()} и vin {granta.Vin}  Копия Granta: цвет {copyGranta.GetColorId()} и vin {copyGranta.Vin}");
            Console.WriteLine($"Созданная 3008: цвет {p3008.GetColorId()} и vin {p3008.Vin}  Копия 3008: цвет {copyP3008.GetColorId()} и vin {copyP3008.Vin}");
            Console.WriteLine($"Созданная 408: цвет {p408.GetColorId()} и vin {p408.Vin}  Копия 408: цвет {copyP408.GetColorId()} и vin {copyP408.Vin}");
            Console.WriteLine($"Созданная Octavia: цвет {octavia.GetColorId()} и vin {octavia.Vin}  Копия Octavia: цвет {copyOctavia.GetColorId()} и vin {copyOctavia.Vin}");
            Console.WriteLine($"Созданная Rapid: цвет {rapid.GetColorId()} и vin {rapid.Vin}  Копия Rapid: цвет {copyRapid.GetColorId()} и vin {copyRapid.Vin}");

            Console.WriteLine("\nИзменяем vin у оригиналов\n");

            vesta.Vin = 123456789;
            granta.Vin = 123456789;
            p3008.Vin = 123456789;
            p408.Vin = 123456789;
            octavia.Vin = 123456789;
            rapid.Vin = 123456789;

            Console.WriteLine($"Созданная Vesta: vin {vesta.Vin}  Копия Vesta: vin {copyVesta.Vin}");
            Console.WriteLine($"Созданная Granta: vin {granta.Vin}  Копия Granta: vin {copyGranta.Vin}");
            Console.WriteLine($"Созданная 3008: vin {p3008.Vin}  Копия 3008: vin {copyP3008.Vin}");
            Console.WriteLine($"Созданная 408: vin {p408.Vin}  Копия 408: vin {copyP408.Vin}");
            Console.WriteLine($"Созданная Octavia: vin {octavia.Vin}  Копия Octavia: vin {copyOctavia.Vin}");
            Console.WriteLine($"Созданная Rapid: vin {rapid.Vin}  Копия Rapid: vin {copyRapid.Vin}");
        }
    }
}
